const fs = require('fs')
const path = require('path')
const dataPath = path.join(__dirname, '../data/');

const locations = JSON.parse(fs.readFileSync(`${dataPath}module-locations.json`, 'utf8'))
const blacklistedLocations = JSON.parse(fs.readFileSync(`${dataPath}blacklisted-locations.json`, 'utf8'))
// const localBaseUrl = '{{localBaseUrl}}' // for dev
const localBaseUrl = 'https://connect-app-toolkit.prod.atl-paas.net'  // for release

const atlassianConnectJson = {
    "key": "com.atlassian.connect.app-toolkit",
    "name": "Connect App Toolkit",
    "description": "App that helps you build Apps.",
    "vendor": {
        "name": "Atlassian",
        "url": "https://www.atlassian.com"
    },
    "baseUrl": localBaseUrl,
    "links": {
        "self": `${localBaseUrl}/atlassian-connect.json`,
        "homepage": `${localBaseUrl}/atlassian-connect.json`
    },
    "enableLicensing": false,
    "authentication": {
        "type": "none"
    },
    "scopes": [
        "read",
        "write",
        "delete"
    ],
}

const modules = {
    webItems: [],
    webPanels: [],
    webSections: []
}

const loggedInCondition = {
    "condition": "user_is_logged_in"
}

locations.webPanels.forEach((location, i) => {
    if (!blacklistedLocations.includes(location)) {
        modules.webPanels.push(createWebPanel(location, i))
    }
})

locations.webSections.forEach((location, i) => {
    if (!blacklistedLocations.includes(location)) {
        modules.webSections.push(createWebSection(location, i))
    }
})

locations.webItems.forEach((location, i) => {
    if (!blacklistedLocations.includes(location)) {
        modules.webItems.push(createWebItem(location, i))
        modules.webItems.push(createWebItem(location, 1000 + i, true))
        modules.webItems.push(createWebItem(location, 2001 + i, true))
        modules.webItems.push(createWebItem(location, 3002 + i, true))
        modules.webItems.push(createWebItem(location, 4003 + i, true))
        modules.webItems.push(createWebItem(location, 5004 + i, true))
    }
})

atlassianConnectJson["modules"] = modules
const acJsonStr = JSON.stringify(atlassianConnectJson);

fs.writeFileSync("atlassian-connect-generated.json", acJsonStr, "utf-8")

// console.log(JSON.stringify(atlassianConnectJson))


function validatorPropertyEnabledCondition(isPopulatorItem = false) {
    return {
        "condition": "entity_property_exists",
        "params": {
            "entity": "user",
            "propertyKey": "com.atlassian.connect.addon.validator.modules." + (isPopulatorItem ? "populate" : "show"),
            "value": "true"
        }
    }
}

function createWebItem(location, index = 0, isPopulatorItem = false, weight = 9999) {
    const iconUrl = (location === "system.top.navigation.bar") ? "/icons/cat_icon_global.svg" : "/icons/cat_icon.svg"
    const isMainAppToolkitItem = location === "system.top.navigation.bar" && !isPopulatorItem
    const name = isMainAppToolkitItem ? "App Toolkit" : location
    const condition = isMainAppToolkitItem ? loggedInCondition : validatorPropertyEnabledCondition(isPopulatorItem)

    return {
        "url": "/display-web-item.html",
        "location": location,
        "context": "addon",
        "weight": weight,
        "target": {
            "type": "dialog",
            "options": {
                "chrome": false
            }
        },
        "tooltip": {
            "value": location
        },
        "key": "connect-addon-validator-generated-item-" + index,
        "name": {
            "value": name
        },
        "icon": {
            "url": iconUrl,
            "width": 32,
            "height": 32,
        },
        "conditions": [condition]
    }
}

function createWebPanel(location, index, weight = 999) {
    return {
        "url": "/display-web-panel.html?web-panel=" + location,
        "location": location,
        "layout": {
            "width": "100%",
            "height": "10px"
        },
        "weight": weight,
        "key": "connect-addon-validator-generated-panel-" + index,
        "name": {
            "value": "Web-Panel: " + location
        },
        "conditions": [validatorPropertyEnabledCondition()]
    }
}


function createWebSection(location, index, weight = 9999) {
    const webSectionKey = "connect-addon-validator-generated-section-" + index
    modules.webItems.push(createWebItem(webSectionKey, 10000 + index))

    return {
        "location": location,
        "weight": weight,
        "key": webSectionKey,
        "name": {
            "value": "Web-Section: " + location
        },
        "conditions": [validatorPropertyEnabledCondition()]
    }
}
