# Connect App Toolkit
Connect App Toolkit (CAT) is an App that helps you build Apps for the Atlassian Marketplace.

# Features
* Shows the extension point keys in Jira Cloud.

## How to use

### Getting started
Make sure you have yarn installed, then run *yarn install*

Create a file called credentials.json, and add your dev Cloud instance credentials there.
Look into credentials.json.sample to see the format.

run *yarn start*

### Creating a new atlassian-connect.json
CAT includes data about all extension points in Jira Cloud. This data can be used to generate atlassian-connect.json to highlight the keys for web items, panels and sections.
To do this look into scripts/generate-ac-json.js for the details.

run *yarn generate* to create atlassian-connection-generated.json file, and copy it to atlassian-connect.json.

## Links
CAT is based on [Atlassian Connect Express (ACE) template](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).
[Atlassian Marketplace](http://marketplace.atlassian.com/)
